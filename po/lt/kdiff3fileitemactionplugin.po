# Lithuanian translations for l package.
# Copyright (C) 2011 This_file_is_part_of_KDE
# This file is distributed under the same license as the l package.
# Automatically generated, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: kdiff3fileitemactionplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-13 00:49+0000\n"
"PO-Revision-Date: 2011-08-02 05:07+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: lt\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"

#: kdiff3fileitemaction.cpp:82
#, kde-format
msgctxt "Contexualmenu title"
msgid "KDiff3..."
msgstr ""

#: kdiff3fileitemaction.cpp:107
#, kde-format
msgctxt "Contexualmenu option"
msgid "Compare with %1"
msgstr ""

#: kdiff3fileitemaction.cpp:113
#, kde-format
msgctxt "Contexualmenu option"
msgid "Merge with %1"
msgstr ""

#: kdiff3fileitemaction.cpp:119
#, kde-format
msgctxt "Contexualmenu option"
msgid "Save '%1' for later"
msgstr ""

#: kdiff3fileitemaction.cpp:125
#, kde-format
msgctxt "Contexualmenu option"
msgid "3-way merge with base"
msgstr ""

#: kdiff3fileitemaction.cpp:132
#, kde-format
msgctxt "Contexualmenu option"
msgid "Compare with..."
msgstr ""

#: kdiff3fileitemaction.cpp:145
#, kde-format
msgctxt "Contexualmenu option to cleat comparison list"
msgid "Clear list"
msgstr ""

#: kdiff3fileitemaction.cpp:153
#, kde-format
msgctxt "Contexualmenu option"
msgid "Compare"
msgstr ""

#: kdiff3fileitemaction.cpp:159
#, kde-format
msgctxt "Contexualmenu option"
msgid "3 way comparison"
msgstr ""

#: kdiff3fileitemaction.cpp:163
#, kde-format
msgctxt "Contexualmenu option"
msgid "About KDiff3 menu plugin..."
msgstr ""

#: kdiff3fileitemaction.cpp:274
#, kde-format
msgid "KDiff3 File Item Action Plugin: Copyright (C) 2011 Joachim Eibl\n"
msgstr ""

#: kdiff3fileitemaction.cpp:275
#, kde-format
msgid ""
"Using the context menu extension:\n"
"For simple comparison of two selected files choose \"Compare\".\n"
"If the other file is somewhere else \"Save\" the first file for later. It "
"will appear in the \"Compare with...\" submenu. Then use \"Compare With\" on "
"the second file.\n"
"For a 3-way merge first \"Save\" the base file, then the branch to merge and "
"choose \"3-way merge with base\" on the other branch which will be used as "
"destination.\n"
"Same also applies to folder comparison and merge."
msgstr ""

#: kdiff3fileitemaction.cpp:283
#, kde-format
msgid "About KDiff3 File Item Action Plugin"
msgstr ""
