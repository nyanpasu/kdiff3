# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Last-Translator: Stefan Asserhall <stefan.asserhall@bredband.net>, 2011, 2018, 2019, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-13 00:49+0000\n"
"PO-Revision-Date: 2022-08-03 10:30+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.08.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: kdiff3fileitemaction.cpp:82
#, fuzzy, kde-format
#| msgid "KDiff3..."
msgctxt "Contexualmenu title"
msgid "KDiff3..."
msgstr "Kdiff3..."

#: kdiff3fileitemaction.cpp:107
#, fuzzy, kde-format
#| msgid "Compare with %1"
msgctxt "Contexualmenu option"
msgid "Compare with %1"
msgstr "Jämför med %1"

#: kdiff3fileitemaction.cpp:113
#, fuzzy, kde-format
#| msgid "Merge with %1"
msgctxt "Contexualmenu option"
msgid "Merge with %1"
msgstr "Sammanfoga med %1"

#: kdiff3fileitemaction.cpp:119
#, fuzzy, kde-format
#| msgid "Save '%1' for later"
msgctxt "Contexualmenu option"
msgid "Save '%1' for later"
msgstr "Spara '%1' till senare"

#: kdiff3fileitemaction.cpp:125
#, fuzzy, kde-format
#| msgid "3-way merge with base"
msgctxt "Contexualmenu option"
msgid "3-way merge with base"
msgstr "Trevägs sammanfogning med bas"

#: kdiff3fileitemaction.cpp:132
#, fuzzy, kde-format
#| msgid "Compare with..."
msgctxt "Contexualmenu option"
msgid "Compare with..."
msgstr "Jämför med..."

#: kdiff3fileitemaction.cpp:145
#, kde-format
msgctxt "Contexualmenu option to cleat comparison list"
msgid "Clear list"
msgstr "Rensa lista"

#: kdiff3fileitemaction.cpp:153
#, fuzzy, kde-format
#| msgctxt "Contexualmenu option "
#| msgid "Compare"
msgctxt "Contexualmenu option"
msgid "Compare"
msgstr "Jämför"

#: kdiff3fileitemaction.cpp:159
#, fuzzy, kde-format
#| msgid "3 way comparison"
msgctxt "Contexualmenu option"
msgid "3 way comparison"
msgstr "Trevägs jämförelse"

#: kdiff3fileitemaction.cpp:163
#, fuzzy, kde-format
#| msgid "About KDiff3 menu plugin..."
msgctxt "Contexualmenu option"
msgid "About KDiff3 menu plugin..."
msgstr "Om Kdiff3:s menyinsticksprogram..."

#: kdiff3fileitemaction.cpp:274
#, kde-format
msgid "KDiff3 File Item Action Plugin: Copyright (C) 2011 Joachim Eibl\n"
msgstr ""
"Kdiff3-insticksprogram för filobjektåtgärder: Copyright © 2011 Joachim Eibl\n"

#: kdiff3fileitemaction.cpp:275
#, kde-format
msgid ""
"Using the context menu extension:\n"
"For simple comparison of two selected files choose \"Compare\".\n"
"If the other file is somewhere else \"Save\" the first file for later. It "
"will appear in the \"Compare with...\" submenu. Then use \"Compare With\" on "
"the second file.\n"
"For a 3-way merge first \"Save\" the base file, then the branch to merge and "
"choose \"3-way merge with base\" on the other branch which will be used as "
"destination.\n"
"Same also applies to folder comparison and merge."
msgstr ""
"Använda utökningen av den sammanhangsberoende menyn:\n"
"För enkel jämförelse av två utvalda filer, välj \"Jämför\".\n"
"Om den andra filen finns någon annanstans, \"Spara\" den första filen till "
"senare. Den visas i undermenyn \"Jämför med...\". Använd därefter \"Jämför "
"med\" för den andra filen.\n"
"För en trevägs sammanfogning, \"Spara\" först basfilen, därefter grenen som "
"ska sammanfogas och välj \"Trevägs sammanfogning med bas\" i den andra "
"grenen, som kommer att användas som resultat.\n"
"Detsamma gäller också för jämförelse och sammanfogning av kataloger."

#: kdiff3fileitemaction.cpp:283
#, kde-format
msgid "About KDiff3 File Item Action Plugin"
msgstr "Om Kdiff3:s insticksprogram för filobjektåtgärder"
